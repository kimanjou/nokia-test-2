
import 'reflect-metadata';
import 'es6-shim';
//import 'es6-promise';
import 'zone.js';

// Angular 2
import 'angular2/platform/browser';
import 'angular2/platform/common_dom';
import 'angular2/router';
import 'angular2/http';
import 'angular2/core';

// RxJS
import 'rxjs';