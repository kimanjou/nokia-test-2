export const questionList = [
    {
        id: 1,
        title: 'Assessment 1: Iteration',
        subTitle: 'Acceptance Tests',
        txt: 'As a team, before we commit to a Sprint, we know its duration, so we deliver better rhythmic, synchronized value.',
        type: 'radio',
        assertion: [
            {
                id: 1,
                txt: 'Variable, 4 < duration <= 6 weeks',
                value: 2
            },
            {
                id: 2,
                txt: 'Variable, duration <= 4 weeks',
                value: 4
            },
            {
                id: 3,
                txt: 'Constant for last 3 iterations, duration = 1 month',
                value: 5
            },
            {
                id: 4,
                txt: 'Constant for last 3 iterations, duration = 4 weeks',
                value: 6
            },
            {
                id: 5,
                txt: 'Constant for last 3 iterations, duration = 3 weeks',
                value: 8
            },
            {
                id: 6,
                txt: 'Constant for last 3 iterations, duration <= 2 weeks',
                value: 10
            }
        ]
    },
    {
        id: 2,
        title: 'Assessment 2: In-Sprint Testing',
        subTitle: 'Acceptance tests (sum)',
        txt: 'As a team, we take joint responsibility for all testing, so our Sprint product has sufficient quality to be immediately deployable.',
        type: 'checkbox',
        assertion: [
            {
                id: 7,
                txt: 'Team creates some unit tests in-sprint',
                value: 1
            },
            {
                id: 8,
                txt: 'Team creates unit tests for each story in-sprint',
                value: 1
            },
            {

                id: 9,
                txt: 'Team tests each story prior to Sprint Review',
                value: 2
            },
            {
                id: 10,
                txt: 'Team tests each story immediately after coding',
                value: 2
            },
            {
                id: 11,
                txt: 'Team automates feature tests for each new story',
                value: 2
            },
            {
                id: 12,
                txt: 'Build system packages, deploys to stage or live, and runs all automated feature tests at least every 24 hours',
                value: 2
            }
        ]
    }
];