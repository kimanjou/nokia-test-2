import {Component, Directive, Input} from 'angular2/core';
import {RadioButtonState} from 'angular2/common';

@Component({
    selector: 'question',
    template: require('./question.html')(),
    providers: [],
    directives: [],
    pipes: []
})
export class Question {
    @Input('question') question: Object;
    form;

    constructor() {
        console.log(this.question);

        this.form = {
            answer: []
        };

    }
}


