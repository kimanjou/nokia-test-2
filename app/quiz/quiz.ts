import {Component} from 'angular2/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import {Question} from './components/question/question.ts';

import {questionList} from './constants/questions.ts';

@Component({
    selector: 'quiz-app',
    providers: [],
    pipes: [],
    directives: [Question],
    template: require('./quiz.html')(),
})
export class QuizApp {
    questions;
    currentQuestion;
    currentIndex;

    constructor() {
        this.currentIndex = 0;
        this.questions = questionList;
        this.currentQuestion = this.questions[this.currentIndex]
    }

}