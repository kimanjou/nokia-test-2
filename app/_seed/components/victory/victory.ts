import {Component} from 'angular2/core';

@Component({
    selector: 'victory',
    template: require('./victory.html')(),
    providers: [],
    directives: [],
    pipes: []
})
export class Victory {
    constructor() {
        console.log('victory');
    }
}