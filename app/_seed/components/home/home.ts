import {Component} from 'angular2/core';

@Component({
    selector: 'home',
    template: require('./home.html')(),
    providers: [],
    directives: [],
    pipes: []
})
export class Home {

    constructor() {
        console.log('home');
     }

    ngOnInit() {

    }

}