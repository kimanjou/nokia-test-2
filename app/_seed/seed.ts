import {Component} from 'angular2/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import {Home} from './components/home/home.ts';
import {Victory} from './components/victory/victory.ts';
import {About} from './components/about/about.ts';
import {RepoBrowser} from './components/repo-browser/repo-browser.ts';

@Component({
    selector: 'seed-app',
    providers: [],
    pipes: [],
    directives: [ROUTER_DIRECTIVES],
    template: require('./seed.html')(),
})
@RouteConfig([
    { path: '/home', component: Home, name: 'Home', useAsDefault: true },
    { path: '/about', component: About, name: 'About' },
    { path: '/github/...', component: RepoBrowser, name: 'RepoBrowser' },
    { path: '/victory', component: Victory, name: 'Victory' },
])
export class SeedApp {

    constructor() {
        console.log('seed');
        console.log(RepoBrowser);
     }

}