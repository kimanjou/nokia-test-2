module.exports = {
    // See http://brunch.io for documentation.
    files: {
        javascripts: {
            joinTo: {
                'vendor.js': /^(app\/vendor)|(node_modules)/,
                'app.js': /^app\/(?!vendor)/
            }
        },
        stylesheets: {
            joinTo: 'app.css'
        },
        templates: {
            joinTo: 'templates.js'
        }
    },
    npm: {
        enabled: true
    }
}
